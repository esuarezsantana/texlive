# This file is generated. If you need to change it, change the source instead.

# Copyright 2015 Bo Ørsted Andresen
# Copyright 2015 Thomas Witt
# Distributed under the terms of the GNU General Public License v2
# Based in part upon the Gentoo texlive ebuilds which are:
#     Copyright 1999-2015 Gentoo Foundation
TEXLIVE_MODULE_CONTENTS="
context jmn context-account context-algorithmic context-animation context-annotation context-bnf
context-chromato context-construction-plan context-cyrillicnumbers context-degrade
context-fancybreak context-filter context-fixme context-french context-fullpage context-games
context-gantt context-gnuplot context-letter context-lettrine context-lilypond context-mathsets
context-notes-zh-cn context-rst context-ruby context-simplefonts context-simpleslides context-title
context-transliterator context-typearea context-typescripts context-vim context-visualcounter
collection-context
"
TEXLIVE_MODULE_DOC_CONTENTS="context.doc context-account.doc context-animation.doc context-annotation.doc context-bnf.doc
context-chromato.doc context-construction-plan.doc context-cyrillicnumbers.doc context-degrade.doc
context-fancybreak.doc context-filter.doc context-french.doc context-fullpage.doc context-games.doc
context-gantt.doc context-gnuplot.doc context-letter.doc context-lettrine.doc context-lilypond.doc
context-mathsets.doc context-notes-zh-cn.doc context-rst.doc context-ruby.doc
context-simplefonts.doc context-simpleslides.doc context-title.doc context-transliterator.doc
context-typearea.doc context-typescripts.doc context-vim.doc context-visualcounter.doc"
TEXLIVE_MODULE_SRC_CONTENTS="context-visualcounter.source"
require texlive-module
DESCRIPTION="TeXLive ConTeXt and packages"

LICENCES="BSD GPL-1 GPL-2 GPL-3 TeX-other-free public-domain"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""
DEPENDENCIES="
    build+run:
        dev-texlive/texlive-basic[>=2015]
        app-text/texlive-core[xetex]
        dev-texlive/texlive-latex
        dev-texlive/texlive-metapost
    run:
        dev-lang/ruby
"
#PATCHES=( "${FILESDIR}/luacnfspec2013.patch" )

TL_CONTEXT_UNIX_STUBS="contextjit mtxrunjit mtxrun texexec context metatex luatools mtxworks texmfstart"

TEXLIVE_MODULE_BINSCRIPTS=""

for i in ${TL_CONTEXT_UNIX_STUBS} ; do
TEXLIVE_MODULE_BINSCRIPTS="${TEXLIVE_MODULE_BINSCRIPTS} texmf-dist/scripts/context/stubs/unix/$i"
done

# This small hack is needed in order to have a sane upgrade path:
# the new TeX Live 2009 metapost produces this file but it is not recorded in
# any package; when running fmtutil (like texmf-update does) this file will be
# created and cause collisions.

pkg_setup() {
	if [ -f "${ROOT}/var/lib/texmf/web2c/metapost/metafun.log" ]; then
		einfo "Removing ${ROOT}/var/lib/texmf/web2c/metapost/metafun.log"
		rm -f "${ROOT}/var/lib/texmf/web2c/metapost/metafun.log"
	fi
}

# These comes without +x bit set...
src_prepare() {
	# No need to install these .exe
	rm -rf texmf-dist/scripts/context/stubs/{mswin,win64} || die

	texlive-module_src_prepare
}

TL_MODULE_INFORMATION="For using ConTeXt mkII simply use 'texexec' to generate
your documents.
If you plan to use mkIV and its 'context' command to generate your documents,
you have to run 'luatools --generate' as normal user before first use.

More information and advanced options on:
http://wiki.contextgarden.net/TeX_Live_2011"

