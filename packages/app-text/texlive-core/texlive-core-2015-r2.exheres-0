# Copyright 2010, 2011 Ingmar Vanhassel
# Copyright 2013-2015 Thomas Witt
# Distributed under the terms of the GNU General Public License v2
# Based in parts upon the texlive-core-2013.ebuild which is
#   Copyright 1999-2013 Gentoo Foundation

require autotools [ supported_autoconf=[ none ] supported_automake=[ 1.15 1.13 ] ] texlive-common

SUMMARY="The TeXlive distribution"
HOMEPAGE="http://tug.org/texlive"
TL_PN="texlive"
TL_PV="20150521"
DOWNLOADS="mirror://ctan/systems/${TL_PN}/Source/${TL_PN}-${TL_PV}-source.tar.xz"

LICENCES="as-is GPL-2 GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS="cjk doc source X xetex"

TL_CORE_BINEXTRA_MODULES=(
    a2ping adhocfilelist arara asymptote bibtex8 bibtexu bundledoc ctan_chk
    ctanify ctanupload ctie cweb de-macro dtl dtxgen dvi2tty dviasm dvicopy
    dvidvi dviljk dvipng dvipos findhyph fragmaster hook-pre-commit-pkg hyphenex
    installfont lacheck latex-git-log latex2man latexfileversion latexpand
    latexindent ltxfileinfo ltximg listings-ext match_parens mkjobtexmf patgen
    pdfcrop pdftools pfarrei pkfix pkfix-helper purifyeps pythontex seetexk
    sty2dtx synctex texcount texdef texdiff texdirflatten texdoc texfot
    texliveonfly texloganalyser texware tie tpic2pdftex typeoutfileinfo web
    collection-binextra
)
TL_CORE_BINEXTRA_DOC_MODULES=(
    a2ping.doc adhocfilelist.doc arara.doc asymptote.doc bibtex8.doc bibtexu.doc
    bundledoc.doc ctan_chk.doc ctanify.doc ctanupload.doc ctie.doc cweb.doc
    de-macro.doc dtl.doc dtxgen.doc dvi2tty.doc dviasm.doc dvicopy.doc dviljk.doc
    dvipng.doc dvipos.doc findhyph.doc fragmaster.doc hook-pre-commit-pkg.doc
    installfont.doc lacheck.doc latex-git-log.doc latex2man.doc
    latexfileversion.doc latexpand.doc latexindent.doc ltxfileinfo.doc
    ltximg.doc listings-ext.doc match_parens.doc mkjobtexmf.doc patgen.doc
    pdfcrop.doc pdftools.doc pfarrei.doc pkfix.doc pkfix-helper.doc
    purifyeps.doc pythontex.doc seetexk.doc sty2dtx.doc synctex.doc texcount.doc
    texdef.doc texdiff.doc texdirflatten.doc texdoc.doc texfot.doc
    texliveonfly.doc texloganalyser.doc texware.doc tie.doc tpic2pdftex.doc
    typeoutfileinfo.doc web.doc
)
TL_CORE_BINEXTRA_SRC_MODULES=(
    adhocfilelist.source arara.source hyphenex.source listings-ext.source
    mkjobtexmf.source pfarrei.source pythontex.source texdef.source
)

TL_CORE_EXTRA_MODULES=(
    gsftopk hyphen-base tetex texconfig texlive.infra
    ${TL_CORE_BINEXTRA_MODULES[@]}
)
TL_CORE_EXTRA_DOC_MODULES=(
    gsftopk.doc tetex.doc texconfig.doc texlive.infra.doc
    ${TL_CORE_BINEXTRA_DOC_MODULES[@]}
)
TL_CORE_EXTRA_SRC_MODULES=( ${TL_CORE_BINEXTRA_SRC_MODULES[@]} )

for i in ${TL_CORE_EXTRA_MODULES[@]}; do
    DOWNLOADS+=" http://dev.exherbo.org/distfiles/texlive/${PV}/texlive-module-${i}-${PV}.tar.xz"
done

DOWNLOADS+=" doc? ("
for i in ${TL_CORE_EXTRA_MODULES[@]}; do
    DOWNLOADS+=" http://dev.exherbo.org/distfiles/texlive/${PV}/texlive-module-${i}-${PV}.tar.xz"
done
DOWNLOADS+=" ) source? ("
for i in ${TL_CORE_EXTRA_MODULES[@]}; do
    DOWNLOADS+=" http://dev.exherbo.org/distfiles/texlive/${PV}/texlive-module-${i}-${PV}.tar.xz"
done
DOWNLOADS+=" )"

REMOTE_IDS="freecode:${TL_PN}"


RESTRICT="test"

DEPENDENCIES="
    build:
        app-arch/xz
        virtual/pkg-config
        sys-apps/ed
        sys-devel/flex
    build+run:
        app-text/dvipsk[>=5.995_p${TL_PV}]
        app-text/ghostscript [[ note = [ --with-system-libgs ] ]]
        app-text/poppler [[ note = [ --with-system-xpdf ] ]]
        dev-libs/icu:= [[ note = [ --with-system-icu ] ]]
        dev-libs/kpathsea[>=6.2.1_p${TL_PV}] [[ note = [ --with-system-kpathsea ] ]]
        dev-libs/ptexenc[>=1.3.3_p${TL_PV}-r1] [[ note = [ --with-system-ptexenc ] ]]
        dev-libs/zziplib [[ note = [ --with-system-zziplib ] ]]
        dev-tex/luatex[>=0.76]
        media-libs/freetype:2 [[ note = [ --with-system-freetype2 ] ]]
        media-libs/gd [[ note = [ --with-system-gd ] ]]
        media-libs/libpng:= [[ note = [ --with-system-libpng ] ]]
        media-libs/t1lib [[ note = [ --with-system-t1lib ] ]]
        !media-libs/TECkit [[ note = [ use bundled version ] resolution = uninstall-blocked-before ]]
        x11-libs/cairo [[ note = [ --with-system-cairo ] ]]
        x11-libs/pixman:1
        X? (
            x11-libs/libXmu
            x11-libs/libXp
            x11-libs/libXpm
            x11-libs/libICE
            x11-libs/libSM
            x11-libs/libXaw
            x11-libs/libXfont
        )
        xetex? (
            media-libs/fontconfig
            media-libs/freetype:2
        )
    recommendation:
        dev-tex/biblatex-biber [[ note = [ Replacement for BibTeX ] ]]
"

        #app-text/ps2pkm[>=1.5_p20120701]
        #dev-tex/bibtexu[>=3.71_p20130530]
            #media-gfx/graphite2
            #x11-libs/harfbuzz
        #xetex? ( app-text/xdvipdfmx[>=0.7.9_p20130530] ) ???

WORK="${WORKBASE}/${TL_PN}-${TL_PV}-build/"
#DEFAULT_SRC_PREPARE_PATCHES=( -p0 "${FILES}/texmfcnflua2013.patch" )
ECONF_SOURCE="${WORKBASE}/${TL_PN}-${TL_PV}-source/"

DEFAULT_SRC_COMPILE_PARAMS=( AR=${AR} )

pkg_pretend() {
    texlive_leftover_warning "${ROOT}"etc/texmf/web2c/updmap.cfg.d/updmap-hdr.cfg
}

src_unpack() {
    default
    edo mkdir "${WORK}"

    option doc || edo rm -rf texmf-dist/doc
}

src_prepare() {
    edo cd "${WORKBASE}"

    texlive-common_reloc_target

    edo cd "${ECONF_SOURCE}"

    # Find the prefixed strings command
    edo sed -e "s/if strings/if $(exhost --tool-prefix)strings/" \
            -i libs/cairo/m4/float.m4
    edo autoreconf libs/cairo

    default

    #FIXME GENTOO PATCH
    # Manually regenerate all Makefile.ams touched by our patches.
    # automake doesn't recurse, autoreconf takes too long.
    #edo autoreconf texk/cjkutils/ texk/dvipdfmx/ texk/dvipsk/ \
                   #texk/gsftopk/ texk/kpathsea/ texk/lcdf-typetools/ \
                   #texk/texlive/ texk/xdvik/ texk/xdvipdfmx/
}

src_configure() {
    local myconf=()

    myconf=(
        --with-banner-add="/Exherbo ${PNVR}"
        # If enabled, build for a TeX Live binary distribution as shipped by the TeX user groups
        --disable-native-texlive-build
        # Don’t install executables, binaries into platform dependent subdirectories of bindir, libdir
        --disable-multiplatform
        --enable-shared
        --disable-static
        # Terminate the build if a requested feature can’t be enabled due to missing dependencies
        --disable-missing

        # Fails for some subdirectories
        --hates=docdir

        # We don’t currently have freetype:1, this is only used by texk/ttf2pk, which we disable
        --with-system-{cairo,freetype2,gd,icu,kpathsea,libgs,pixman,libpng,ptexenc,poppler,t1lib,xpdf,zlib,zziplib}
        --without-system-{graphite2,harfbuzz,teckit}
        --with-freetype2-include=/usr/$(exhost --target)/include
        --with-kpathsea-includes=/usr/$(exhost --target)/include
        --with-petexenc-include=/usr/$(exhost --target)/include

        --without-freetype

        # Following features disabled temporarily until needed
        --disable-lcdf-typetools
        --disable-ttf2pk # requires freetype:1
        --disable-xindy

        --disable-luatex # separate package

        --disable-biber # separate package

        --disable-ps2eps
        --disable-dvipng
        --disable-dvipsk #separate package
        --disable-chktex
        --disable-pdfopen
        --enable-ps2pkm
        --disable-detex
        --disable-ttf2pk
        --disable-ttf2pk2
        --disable-tex4htk
        --disable-cjkutils
        --disable-xdvik
        --disable-dvi2tty
        --disable-dvisvgm
        --disable-vlna

        # TODO system graphite2
        $(option_enable xetex)
        $(option_enable xetex xdvipdfmx)
        #$(option_with xetex graphite2)

        $(option_enable cjk ptex)
        $(option_enable cjk eptex)
        $(option_enable cjk uptex)
        $(option_enable cjk euptex)
        $(option_enable cjk mendexk)
        $(option_enable cjk makejvf)

        $(option_with X x)

        --disable-texdoctk
        --disable-dialog # sys-apps/dialog
        --disable-psutils # app-text/psutils
    )

    econf "${myconf[@]}"
}

src_install() {
    default

    dosym bibtex8 /usr/$(exhost --target)/bin/bibtex

    # FIXME: Comb through /usr/texmf*/scripts for cruft that shouldn't be installed
    # Get rid of tlmgr bits
    edo rm -rf "${IMAGE}"/usr/{$(exhost --target)/bin/tlmgr,texmf/scripts/texlive/}

    edo cp -pPR "${WORKBASE}/"texmf-dist "${IMAGE}"/usr/share/

    dodir /usr/share/tlpkg
    edo cp -pPR "${WORKBASE}/"tlpkg/TeXLive "${IMAGE}"/usr/share/tlpkg/

    dodir /etc/texmf/updmap.d
    edo mv "${IMAGE}/usr/share/texmf-dist/web2c/updmap-hdr.cfg" "${IMAGE}/etc/texmf/updmap.d"
    dodir /etc/texmf/fmtutil.d
    edo mv "${IMAGE}/usr/share/texmf-dist/web2c/fmtutil-hdr.cnf" "${IMAGE}/etc/texmf/fmtutil.d"

    # autogenerated by etexmf-update
    edo rm "${IMAGE}"usr/share/texmf-dist/web2c/updmap.cfg
    edo rm "${IMAGE}"usr/share/texmf-dist/web2c/fmtutil.cnf
    edo rm "${IMAGE}"usr/share/texmf-dist/tex/generic/config/language.dat
    edo rm "${IMAGE}"usr/share/texmf-dist/tex/generic/config/language.dat.lua
    edo rm "${IMAGE}"usr/share/texmf-dist/tex/generic/config/language.def

    texlive-common_handle_config_files

    # handled by dev-tex/glossaries
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/makeglossaries
    # handled by dev-texlive/langgreek
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/mkgrkindex
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/mkgrkindex
    # handled by dev-texlive/langcyrillic
    edo rm "${IMAGE}"usr/share/texmf-dist/scripts/texlive/rubibtex.sh
    edo rm "${IMAGE}"usr/share/texmf-dist/scripts/texlive/rumakeindex.sh
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/rubibtex
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/rumakeindex
    # handled by dev-texlive/texlive-fontutils
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/accfonts
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/dosepsbin
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/epstopdf
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/fontools
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/mf2pt1
    edo rm "${IMAGE}"usr/share/texmf-dist/scripts/texlive/fontinst.sh
    edo rm "${IMAGE}"usr/share/texmf-dist/scripts/texlive/ps2frag.sh
    edo rm "${IMAGE}"usr/share/texmf-dist/scripts/texlive/pslatex.sh
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/afm2afm
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/autoinst
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/dosepsbin
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/epstopdf
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/fontinst
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/mf2pt1
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/mkt1font
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/ot2kpx
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/ps2frag
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/pslatex
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/repstopdf
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/vpl2ovp
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/vpl2vpl
    # handled by dev-texlive/texlive-langindic
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/ebong
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/ebong
    # handled by dev-texlive/texlive-latex
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/context/perl
    nonfatal edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/oberdiek
    # handled by dev-texlive/texlive-latexrecommended
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/thumbpdf
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/thumbpdf
    # handled by dev-texlive/texlive-luatex
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/checkcites
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/lua2dox
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/luaotfload
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/checkcites
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/lua2dox_filter
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/luaotfload-tool
    # handled by dev-texlive/texlive-pstricks
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/cachepic
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/fig4latex
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/mathspic
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/mkpic
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/pedigree-perl
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/pst2pdf
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/pedigree
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/pst2pdf
    # handled by dev-texlive/texlive-latexextra
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/authorindex
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/exceltex
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/perltex
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/pst-pdf
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/splitindex
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/svn-multi
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/vpe
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/yplan
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/authorindex
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/exceltex
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/perltex
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/ps4pdf
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/splitindex
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/svn-multi
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/vpe
    # handled by dev-texlive/texlive-basic
    edo rm -r "${IMAGE}"usr/share/texmf-dist/dvips
    edo rm -r "${IMAGE}"usr/share/texmf-dist/dvipdfmx
    edo rm -r "${IMAGE}"usr/share/texmf-dist/fonts
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/simpdftex
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/texlive/rungs.tlu
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/mptopdf
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/pdfatfi
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/rungs
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/simpdftex
    # handled by dev-texlive/texlive-science
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/ulqda
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/pygmentex
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/ulqda
    # handled by dev-texlive/texlive-music
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/lilyglyphs
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/m-tx
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/musixtex
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/pmx{,chords}
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/lily-glyph-commands
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/lily-image-commands
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/lily-rebuild-pdfs
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/m-tx
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/musixflx
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/musixtex
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/pmx2pdf
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/pmxchords
    # handled by dev-texlive/texlive-games
    edo rm -r "${IMAGE}"/usr/share/texmf-dist/scripts/rubik
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/rubikrotation
    # handled by dev-texlive/texlive-bibtexextra
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/bibexport
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/multibibliography
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/urlbst
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/listbib
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/crossrefware
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/bibexport
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/listbib
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/multibibliography
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/urlbst
    # handled by dev-texlive/texlive-context
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/context
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/context
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/contextjit
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/luatools
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/mtxrun
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/mtxrunjit
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/texexec
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/texmfstart
    # handled by dev-texlive/texlive-langcjk
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/cjk-gs-integrate
    # handled by dev-texlive/texlive-pictures
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/getmap
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/cachepic
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/fig4latex
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/mathspic
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/mkpic
    # handled by dev-texlive/texlive-langjapanese
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/convbkmk
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/jfontmaps
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/ptex2pdf
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/convbkmk
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/ptex2pdf
    # handled by dev-texlive/texlive-langkorean
    edo rm -r "${IMAGE}"usr/share/texmf-dist/scripts/kotex-utils
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/jamo-normalize
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/komkindex
    edo rm "${IMAGE}"usr/$(exhost --target)/bin/ttf2kotexfont

    keepdir /usr/share/texmf-dist/web2c
}

pkg_postinst() {
    etexmf-update
}

pkg_postrm() {
    if [[ -z ${REPLACED_BY_ID} ]]; then
        nonfatal edo rm "${ROOT}"/etc/texmf/web2c/updmap.cfg
        nonfatal edo rm "${ROOT}"/etc/texmf/web2c/fmtutil.cfg
    fi
}

